package com.school.common.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDTO <T>{
    private String status;
    private String message;
    private T data;
}
