package com.school.student.controller;

import com.school.student.dto.ResponseDTO;
import com.school.student.model.Student;
import com.school.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping
    public ResponseDTO<List<Student>> getAllStudents() {
        ResponseDTO<List<Student>> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(studentService.getAllStudents());
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PostMapping
    public ResponseDTO<Student> createStudent(@RequestBody Student student) {
        ResponseDTO<Student> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(studentService.createStudent(student));
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @GetMapping("/{id}")
    public ResponseDTO<Student> getStudentById(@PathVariable("id") Long id) {
        ResponseDTO<Student> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(studentService.getStudentById(id));
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PutMapping("/{id}")
    public ResponseDTO<Student> updateStudent(@PathVariable("id") Long id, @RequestBody Student student) {
        ResponseDTO<Student> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(studentService.updateStudent(id, student));

        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseDTO<Student> deleteStudent(@PathVariable("id") Long id) {
        ResponseDTO<Student> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            studentService.deleteStudent(id);
            responseDTO.setMessage("Student with ID " + id + " has been successfully deleted.");
            studentService.deleteStudent(id);
        }catch (Exception exception){
            responseDTO.setErrorStatusAndMessage(exception.getMessage());
        }
        return responseDTO;
    }
}
