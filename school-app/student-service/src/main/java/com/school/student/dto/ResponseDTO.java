package com.school.student.dto;

import com.school.student.model.Student;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO <T>{
    private int status;
    private String message;
    private T data;

    public void setSuccessStatusAndMessage() {
        this.status = 200;
        this.message = "success";
    }

    public void setErrorStatusAndMessage(String errorMessage) {
        this.status = 500;
        this.message = "error: " + errorMessage;
    }

}
