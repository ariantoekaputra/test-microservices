package com.school.subject.controller;

import com.school.subject.dto.ResponseDTO;
import com.school.subject.model.Subject;
import com.school.subject.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subjects")
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @GetMapping
    public ResponseDTO<List<Subject>> getSubjects() {
        ResponseDTO<List<Subject>> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(subjectService.getAllSubjects());
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @GetMapping("/{id}")
    public ResponseDTO<Subject> getSubject(@PathVariable Long id) {
        ResponseDTO<Subject> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(subjectService.getSubjectById(id));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PostMapping
    public ResponseDTO<Subject> createSubject(@RequestBody Subject subject) {
        ResponseDTO<Subject> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(subjectService.saveSubject(subject));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PutMapping("/{id}")
    public ResponseDTO<Subject> updateSubject(@PathVariable Long id, @RequestBody Subject subject) {
        ResponseDTO<Subject> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(subjectService.updateSubject(id,subject));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseDTO<Subject> deleteSubject(@PathVariable Long id) {
        ResponseDTO<Subject> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            subjectService.deleteSubject(id);
            responseDTO.setMessage("Subject deleted successfully");
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }
}
