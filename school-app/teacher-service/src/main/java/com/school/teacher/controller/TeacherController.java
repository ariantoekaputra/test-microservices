package com.school.teacher.controller;

import com.school.teacher.dto.ResponseDTO;
import com.school.teacher.model.Teacher;
import com.school.teacher.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teachers")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping
    public ResponseDTO<List<Teacher>> getAllTeachers() {
        ResponseDTO<List<Teacher>> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(teacherService.getAllTeachers());
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PostMapping
    public ResponseDTO<Teacher> createTeacher(@RequestBody Teacher teacher) {
        ResponseDTO<Teacher> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(teacherService.saveTeacher(teacher));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @GetMapping("/{id}")
    public ResponseDTO<Teacher> getTeacher(@PathVariable Long id) {
        ResponseDTO<Teacher> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(teacherService.getTeacherById(id));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PutMapping("/{id}")
    public ResponseDTO<Teacher> updateTeacher(@PathVariable Long id, @RequestBody Teacher teacher) {
        ResponseDTO<Teacher> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(teacherService.updateTeacher(id, teacher));
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseDTO<Teacher> deleteTeacher(@PathVariable Long id) {
        ResponseDTO<Teacher> responseDTO = new ResponseDTO<>();
        try {
            teacherService.deleteTeacher(id);
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setMessage("Teacher with ID " + id + " has been successfully deleted.");
        }catch (Exception e) {
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }
}
