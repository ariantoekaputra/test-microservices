package com.school.class_service.service;

import com.school.class_service.model.ClassModel;
import com.school.class_service.repository.ClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClassService {
    @Autowired
    private ClassRepository classRepository;

    public List<ClassModel> getAllClass() {
        return classRepository.findAll();
    }
    public ClassModel getClassById(Long id) {
        return classRepository.findById(id).orElse(null);
    }
    public ClassModel createClass(ClassModel classModel) {
        return classRepository.save(classModel);
    }
    public ClassModel updateClassById(Long id, ClassModel classModel) {
        if (classRepository.existsById(id)){
            classModel.setId(id);
            return classRepository.save(classModel);
        }
        return null;
    }
    public void deleteClassById(Long id) {
        classRepository.deleteById(id);
    }
}
