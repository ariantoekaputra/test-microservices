package com.school.class_service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO<T>{
    private int status;
    private String message;
    private T data;

    public void setSuccessStatusAndMessage() {
        this.status = 200;
        this.message = "success";
    }

    public void setErrorStatusAndMessage(String errorMessage) {
        this.status = 500;
        this.message = "error: " + errorMessage;
    }

}
