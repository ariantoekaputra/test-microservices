package com.school.class_service.controller;

import com.school.class_service.dto.ResponseDTO;
import com.school.class_service.model.ClassModel;
import com.school.class_service.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/class")
public class ClassController {
    @Autowired
    private ClassService classService;

    @GetMapping
    public ResponseDTO<List<ClassModel>> getAllClasses() {
        ResponseDTO<List<ClassModel>> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(classService.getAllClass());
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @GetMapping("/{id}")
    public ResponseDTO<ClassModel> getClassById(@PathVariable("id") Long id) {
        ResponseDTO<ClassModel> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(classService.getClassById(id));
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PostMapping
    public ResponseDTO<ClassModel> addClass(@RequestBody ClassModel classModel) {
        ResponseDTO<ClassModel> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(classService.createClass(classModel));
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @PutMapping("/{id}")
    public ResponseDTO<ClassModel> updateClass(@PathVariable("id") Long id, @RequestBody ClassModel classModel) {
        ResponseDTO<ClassModel> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            responseDTO.setData(classService.updateClassById(id, classModel));
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseDTO<ClassModel> deleteClass(@PathVariable("id") Long id) {
        ResponseDTO<ClassModel> responseDTO = new ResponseDTO<>();
        try {
            responseDTO.setSuccessStatusAndMessage();
            classService.deleteClassById(id);
            responseDTO.setMessage("Class with ID " + id + " has been successfully deleted.");
        }catch (Exception e){
            responseDTO.setErrorStatusAndMessage(e.getMessage());
        }
        return responseDTO;
    }
}
